from django.shortcuts import render

# Create your views here.
def chat(request):
  return render(request, 'chat_user.html')

def create_update_barang(request):
  return render(request,'CU_barang.html')

def list_barang(request):
  return render(request, 'daftar_barang.html')

def chat_list(request):
  return render(request, 'daftar_chat_admin.html')

def list_level(request):
  return render(request, 'daftar_level.html')

def list_order(request):
  return render(request, 'daftar_pesanan.html')

def detail_barang(request):
  return render(request, 'detail_barang.html')

def form_order(request):
  return render(request, 'form_pemesanan.html')

def form_add_level(request):
  return render(request, 'form_tambah_level.html')

def form_update_level(request):
  return render(request, 'form_update_level.html')

def form_update_pesanan(request):
  return render(request, 'form_update_pesanan.html')

def sign_in(request):
  return render(request, 'sign_in.html')