from django.urls import re_path, path, include
from django.conf import settings
from mainApp import views

# from .views import index

urlpatterns = [
  re_path(r'^chat', views.chat),
  re_path(r'^create-update-barang', views.create_update_barang),
  re_path(r'^list-barang', views.list_barang),
  re_path(r'^chat-list', views.chat_list),
  re_path(r'^list-level', views.list_level),
  re_path(r'^list-order', views.list_order),
  re_path(r'^detail-barang', views.detail_barang),
  re_path(r'^form-order', views.form_order),
  re_path(r'^form-add-level', views.form_add_level),
  re_path(r'^form-update-level', views.form_update_level),
  re_path(r'^form-update-pesanan', views.form_update_pesanan),
  re_path(r'^sign-in', views.sign_in),
]
